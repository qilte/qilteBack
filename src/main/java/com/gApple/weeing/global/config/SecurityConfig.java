package com.gApple.weeing.global.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
public class SecurityConfig {
  @Bean
  public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
    http.authorizeHttpRequests(request -> request
            .requestMatchers("/**").permitAll() // WebSecurity ant 어쩌고였음
            .anyRequest().authenticated()
        )
        .httpBasic(withDefaults()); // HttpSecurity
    http.sessionManagement((sessionManagement) ->
        sessionManagement.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
    );
    http.csrf(AbstractHttpConfigurer::disable);
    http.formLogin((formLogin) -> formLogin
        .disable()
    );
    return http.build();
  }
}