package com.gApple.weeing.domain.member.service;

import com.gApple.weeing.domain.member.domain.Member;

import java.util.List;

public interface MemberService {
  void join(Member vo);
  List<Member> findAll();
  public Member findMember(String email);
}
