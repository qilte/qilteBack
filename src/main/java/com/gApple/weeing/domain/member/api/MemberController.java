package com.gApple.weeing.domain.member.api;

import com.gApple.weeing.domain.form.JoinForm;
import com.gApple.weeing.domain.form.LoginForm;
import com.gApple.weeing.domain.member.domain.Member;
import com.gApple.weeing.domain.member.domain.Role;
import com.gApple.weeing.domain.member.service.MemberServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
public class MemberController {
  private final MemberServiceImpl memberServiceImpl; // field autowired는 추천되지 않
  private PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

  @Autowired
  public MemberController(MemberServiceImpl memberServiceImpl){
    this.memberServiceImpl = memberServiceImpl;
  }

  @PostMapping ("/join")
  public String join(@RequestBody JoinForm form) {
    Member vo = new Member(form.username, form.password, form.email, Role.ROLE_USER);
    vo.setPassword(passwordEncoder.encode(vo.getPassword()));

    try {
      memberServiceImpl.join(vo);
      return "Successful Signup";
    } catch (Exception e){
      return "Failed Signup";
    }
  }

  @PostMapping("/login")
  public Member find(@RequestBody LoginForm form){
    Member vo = memberServiceImpl.findMember(form.email);
    boolean passwordMatch = passwordEncoder.matches(form.password, vo.getPassword());

    if(passwordMatch == true){
      return vo;
    } else return new Member();
  }
}
