package com.gApple.weeing.domain.member.service;

import com.gApple.weeing.domain.member.repository.MemberRepository;
import com.gApple.weeing.domain.member.domain.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MemberServiceImpl {
  private final MemberRepository memberRepository;

  @Autowired
  public MemberServiceImpl(MemberRepository memberRepository){
    this.memberRepository = memberRepository;
  }

  public void join(Member vo){
    memberRepository.save(vo);
  }

  public List<Member> findAll() {
    List<Member> vo = memberRepository.findAll();
    return vo;
  }

//  @Transactional(Isolation.SERIALIZABLE)
  public Member findMember(String email) {
    List<Member> vo = memberRepository.findByEmail(email);
    return vo.get(0);
  }
}
