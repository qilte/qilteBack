package com.gApple.weeing.domain.member.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
@Table
@Entity(name = "member")
public class Member {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long userid;

  @NotNull
  private String password;

  @NotNull
  @Column(unique = true)
  private String username;

  @NotNull
  @Pattern(regexp = "[A-Za-z0-9]+@[A-Za-z0-9]+\\.[A-Za-z]+", message = "이메일 형식이 유효하지 않습니다.")
  @Column(unique = true)
  private String email;

  private Role role;

  public Member(String username, String password, String email, Role role){
    this.username = username;
    this.password = password;
    this.email = email;
    this.role = role;
  }

  public Member() {
  }

  public Long getUserid() {
    return userid;
  }

  public void setUserid(Long userid) {
    this.userid = userid;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Role getRole() {
    return role;
  }

  public void setRole(Role role) {
    this.role = role;
  }
}