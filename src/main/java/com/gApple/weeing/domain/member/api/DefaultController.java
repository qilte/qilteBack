package com.gApple.weeing.domain.member.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController // Return View
public class DefaultController {
  @GetMapping({"", "/"})
  public String defaultController(){
    return "<h1>home</h1>";
  }
}
